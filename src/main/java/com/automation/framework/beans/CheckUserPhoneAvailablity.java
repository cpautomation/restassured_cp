package com.automation.framework.beans;

import com.automation.framework.beans.request.RequestBean;

public class CheckUserPhoneAvailablity extends RequestBean {
	private String mobileNumber;
	private String mobileIsdCode;
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMobileIsdCode() {
		return mobileIsdCode;
	}
	public void setMobileIsdCode(String mobileIsdCode) {
		this.mobileIsdCode = mobileIsdCode;
	}
	
	
	
}