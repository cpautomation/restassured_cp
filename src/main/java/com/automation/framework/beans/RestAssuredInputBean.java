package com.automation.framework.beans;

import java.util.ArrayList;
import java.util.List;

public class RestAssuredInputBean {
	private String contentType;
	private String acceptType;
	private String methodType;
	private String reqBean;
	private String resBean;
	private String url;
	private String reqFormatter;
	private String resFormatter;
	private String reqBody;
	private int expectedCode;
	private String expectedValue;
	private List<ParamsBean> expectedValues;
	private String carryForwardFields;
	private String headerCarryForwardFields;
	private List<ParamsBean> reqBodyParams = new ArrayList<ParamsBean>();
	private boolean isHeader;
	private String customParam;
	
	
	public List<ParamsBean> getExpectedValues() {
		return expectedValues;
	}

	public void setExpectedValues(List<ParamsBean> expectedValues) {
		this.expectedValues = expectedValues;
	}

	public String getCustomParam() {
		return customParam;
	}

	public void setCustomParam(String customParam) {
		this.customParam = customParam;
	}

	public RestAssuredInputBean() {
		super();
	}

	public RestAssuredInputBean(String contentType, String acceptType, String methodType, String reqBean,
			String resBean,String url,String reqFormatter,
			String resFormatter, String reqBody, int expectedCode, String expectedValue, String carryForwardFields) {
		super();
		this.contentType = contentType;
		this.acceptType = acceptType;
		this.methodType = methodType;
		this.reqBean = reqBean;
		this.resBean = resBean;
		this.url = url;
		this.reqFormatter = reqFormatter;
		this.resFormatter = resFormatter;
		this.reqBody = reqBody;
		this.expectedCode = expectedCode;
		this.expectedValue = expectedValue;
		this.carryForwardFields = carryForwardFields;
	}

	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getAcceptType() {
		return acceptType;
	}
	public void setAcceptType(String acceptType) {
		this.acceptType = acceptType;
	}

	public String getMethodType() {
		return methodType;
	}
	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public String getReqBean() {
		return reqBean;
	}
	public void setReqBean(String reqBean) {
		this.reqBean = reqBean;
	}

	public String getResBean() {
		return resBean;
	}
	public void setResBean(String resBean) {
		this.resBean = resBean;
	}

	
    public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	
	public String getReqFormatter() {
		return reqFormatter;
	}
	public void setReqFormatter(String reqFormatter) {
		this.reqFormatter = reqFormatter;
	}

	public String getResFormatter() {
		return resFormatter;
	}
	public void setResFormatter(String resFormatter) {
		this.resFormatter = resFormatter;
	}

	public String getReqBody() {
		return reqBody;
	}
	public void setReqBody(String reqBody) {
		this.reqBody = reqBody;
	}

	public int getExpectedCode() {
		return expectedCode;
	}
	public void setExpectedCode(int expectedCode) {
		this.expectedCode = expectedCode;
	}

	public String getExpectedValue() {
		return expectedValue;
	}
	public void setExpectedValue(String expectedValue) {
		this.expectedValue = expectedValue;
	}

	public String getCarryForwardFields() {
		return carryForwardFields;
	}
	public void setCarryForwardFields(String carryForwardFields) {
		this.carryForwardFields = carryForwardFields;
	}
	
	public void setHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}
	public boolean isHeader() {
		return isHeader;
	}
	
	public void setReqBodyParams(List<ParamsBean> reqBodyParams) {
		this.reqBodyParams = reqBodyParams;
	}
	public List<ParamsBean> getReqBodyParams() {
		return reqBodyParams;
	}
	
	public void setHeaderCarryForwardFields(String headerCarryForwardFields) {
		this.headerCarryForwardFields = headerCarryForwardFields;
	}
	public String getHeaderCarryForwardFields() {
		return headerCarryForwardFields;
	}

	@Override
	public String toString() {
		return "RestAssuredInputBean [contentType=" + contentType + ", acceptType=" + acceptType + ", methodType="
				+ methodType + ", reqBean=" + reqBean + ", resBean=" + resBean + ",  url=" + url + ", reqFormatter="
				+ reqFormatter + ", resFormatter=" + resFormatter + ", reqBody=" + reqBody + ", expectedCode="
				+ expectedCode + ", expectedValue=" + expectedValue + ", carryForwardFields=" + carryForwardFields
				+ ", reqBodyParams=" + reqBodyParams + ", isHeader=" + isHeader + "]";
	}
	
}