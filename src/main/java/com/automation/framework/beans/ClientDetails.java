package com.automation.framework.beans;

import com.automation.framework.beans.request.RequestBean;

public class ClientDetails extends RequestBean {
	private String partnerID;
	private String secretCode;
	
	public ClientDetails() {
		super();
	}

	public ClientDetails(String partnerID, String secretCode) {
		super();
		this.partnerID = partnerID;
		this.secretCode = secretCode;
	}

	public String getPartnerID() {
		return partnerID;
	}
	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getSecretCode() {
		return secretCode;
	}
	public void setSecretCode(String secretCode) {
		this.secretCode = secretCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((partnerID == null) ? 0 : partnerID.hashCode());
		result = prime * result + ((secretCode == null) ? 0 : secretCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientDetails other = (ClientDetails) obj;
		if (partnerID == null) {
			if (other.partnerID != null)
				return false;
		} else if (!partnerID.equals(other.partnerID))
			return false;
		if (secretCode == null) {
			if (other.secretCode != null)
				return false;
		} else if (!secretCode.equals(other.secretCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClientDetails [partnerID=" + partnerID + ", secretCode=" + secretCode + "]";
	}
}