package com.automation.framework.beans;

import java.util.HashMap;
import java.util.Map;

public class RestAssuredContext {
	private Map<String, String> contextMap = new HashMap<String, String>();
	private Map<String, String> responserMap = new HashMap<String, String>();

	public RestAssuredContext() {
		super();
	}
	
	
	public Map<String, String> getContextMap() {
		return contextMap;
	}
	public void setContextMap(Map<String, String> contextMap) {
		this.contextMap = contextMap;
	}
	
	public void setResponserMap(Map<String, String> responserMap) {
		this.responserMap = responserMap;
	}
	public Map<String, String> getResponserMap() {
		return responserMap;
	}

	@Override
	public String toString() {
		return "RestAssuredContext [contextMap=" + contextMap + "]";
	}
}