package com.automation.framework.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;

import io.restassured.http.ContentType;

public class RestAssuredBean {
	
	private String url;
	private String methodType;
	private ContentType contentType;
	private ContentType acceptType;
	private boolean isHeader;
	private Map<String, String> headers = new HashMap<String, String>();
	private Object requestBody;
	private int httpExpectedCode;
	
	private Map<String,Object> requestParams;
	private String customParam;
	
	
	public String getCustomParam() {
		return customParam;
	}

	public void setCustomParam(String customParam) {
		this.customParam = customParam;
	}

	public RestAssuredBean() {
		super();
	}

	public RestAssuredBean(String url, String methodType, ContentType contentType,
			ContentType acceptType, Map<String, String> headers, Object requestBody, int httpExpectedCode) {
		super();
		
		this.url = url;
		this.methodType = methodType;
		this.contentType = contentType;
		this.acceptType = acceptType;
		this.headers = headers;
		this.requestBody = requestBody;
		this.httpExpectedCode = httpExpectedCode;
		
		
	}


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	public ContentType getAcceptType() {
		return acceptType;
	}

	public void setAcceptType(ContentType acceptType) {
		this.acceptType = acceptType;
	}

	public Object getRequestBody() {
		return requestBody;
	}

	public void setRequestBody(Object requestBody) {
		this.requestBody = requestBody;
	}

	public int getHttpExpectedCode() {
		return httpExpectedCode;
	}

	public void setHttpExpectedCode(int httpExpectedCode) {
		this.httpExpectedCode = httpExpectedCode;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}
	public Map<String, String> getHeaders() {
		return headers;
	}
	
	public void setHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}
	public boolean isHeader() {
		return isHeader;
	}

	
	public Map<String,Object> getRequestParams() {
		return requestParams;
	}

	public void setRequestParams(Map<String,Object> requestParams) {
		this.requestParams = requestParams;
	}

	@Override
	public String toString() {
		return "RestAssuredBean [url=" + url + ", methodType=" + methodType + ", contentType="
				+ contentType + ", acceptType=" + acceptType + ", isHeader=" + isHeader + ", headers=" + headers
				+ ", requestBody=" + requestBody + ", httpExpectedCode=" + httpExpectedCode + ",requestParams="
				+ requestParams + ", customParam=" + customParam + "]";
	}
}