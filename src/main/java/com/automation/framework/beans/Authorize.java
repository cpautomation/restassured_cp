package com.automation.framework.beans;

import com.automation.framework.beans.request.RequestBean;

public class Authorize extends RequestBean {
	private String grantType;
	private ClientDetails clientDetails;
	
	public Authorize() {
		super();
	}

	public Authorize(String grantType, ClientDetails clientDetails) {
		super();
		this.grantType = grantType;
		this.clientDetails = clientDetails;
	}

	public String getGrantType() {
		return grantType;
	}
	public void setGrantType(String grantType) { 
		this.grantType = grantType;
	}

	public ClientDetails getClientDetails() {
		return clientDetails;
	}
	public void setClientDetails(ClientDetails clientDetails) {
		this.clientDetails = clientDetails;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientDetails == null) ? 0 : clientDetails.hashCode());
		result = prime * result + ((grantType == null) ? 0 : grantType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Authorize other = (Authorize) obj;
		if (clientDetails == null) {
			if (other.clientDetails != null)
				return false;
		} else if (!clientDetails.equals(other.clientDetails))
			return false;
		if (grantType == null) {
			if (other.grantType != null)
				return false;
		} else if (!grantType.equals(other.grantType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Authorize [grantType=" + grantType + ", clientDetails=" + clientDetails + "]";
	}
	
}