package com.automation.framework.beans;

public class ParamsBean {
	private String name;
	private String value;
	private String parentName;
	
	
	public ParamsBean() {
		super();
	}

	public ParamsBean(String name, String value,String parentName) {
		super();
		this.name = name;
		this.value = value;
		this.parentName=parentName;
	}
	

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}

	public void setValue(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return "ParamsBean [name=" + name + ", value=" + value + ", parentName=" + parentName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((parentName == null) ? 0 : parentName.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParamsBean other = (ParamsBean) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (parentName == null) {
			if (other.parentName != null)
				return false;
		} else if (!parentName.equals(other.parentName))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}