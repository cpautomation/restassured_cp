package com.automation.framework.utility;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testng.ITestContext;

import com.automation.framework.beans.RestAssuredBean;
import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.formatter.RequestDataFormatter;
import com.automation.framework.formatter.ResponseDataFormatter;
import com.automation.framework.utility.factory.RequestFormatterFactory;
import com.automation.framework.utility.factory.ResponseFormatterFactory;

import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

@Component
public class RestAssuredDocHelper {
	@Autowired
	private RestAssuredDocFileUploadUtil restAssuredDocFileUploadUtil;
	@Autowired
	private RestAssuredUtil restAssuredUtil;
	@Autowired
	private RequestFormatterFactory requestFormatterFactory;
	@Autowired
	private ResponseFormatterFactory responseFormatterFactory;
	
	/**
	 * Loads the test scenario file and prepare the test scenario bean, for further processing
	 */
	public void doProcess(String fileName, String useCase, RequestSpecification spec, ITestContext context) {
		RestAssuredContext restAssuredContext = restAssuredDocFileUploadUtil.prepareRequestContext();
		RestAssuredInputBean restAssuredInputBean = restAssuredDocFileUploadUtil.init(fileName, useCase, restAssuredContext);
		process(restAssuredInputBean, restAssuredContext, spec, context);
	}
	
	public void process(RestAssuredInputBean restAssuredInputBean, RestAssuredContext restAssuredContext, RequestSpecification spec, ITestContext context) {
		RequestDataFormatter reqFormatter = requestFormatterFactory.getRequestDataFormatter(restAssuredInputBean);
		
		System.out.println(reqFormatter);
		
		try {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("url", reqFormatter.prepareUrl(restAssuredContext, restAssuredInputBean));
		
		map.put("contentType", reqFormatter.prepareContentType(restAssuredContext, restAssuredInputBean));
		map.put("acceptType", reqFormatter.prepareAcceptType(restAssuredContext, restAssuredInputBean));
		map.put("methodType", reqFormatter.prepareMethodType(restAssuredContext, restAssuredInputBean));
		map.put("headers", reqFormatter.prepareRequestHeaders(restAssuredContext, restAssuredInputBean, context));
		if(reqFormatter.prepareMethodType(restAssuredContext, restAssuredInputBean).equals("POST")) 
		{
		map.put("requestBean", reqFormatter.prepareRequestBody(restAssuredContext, restAssuredInputBean,context));
		}
		
		map.put("customParam",reqFormatter.prepareCustomParam(restAssuredContext, restAssuredInputBean));
		
		RestAssuredBean restAssuredBean = restAssuredUtil.prepareAssuredBean(map);
		restAssuredBean.setHttpExpectedCode(restAssuredInputBean.getExpectedCode());
		restAssuredBean.setHeader(restAssuredInputBean.isHeader());
		
		//PROCESS RESPONSE(HANDLE,VALIDATE,SET CARRY FORWARD)
		System.out.println("spec123 "+spec);
		System.out.println("restAssuredBean "+restAssuredBean);
		ValidatableResponse response = restAssuredUtil.prepareAssured(spec, restAssuredBean);
		System.out.println("response "+response);
		ResponseDataFormatter resFormatter = responseFormatterFactory.getResponseDataFormatter(restAssuredInputBean);
		Map<String, Object> responseMap = resFormatter.handleResponse(restAssuredContext, restAssuredInputBean, response);
		
		resFormatter.validateResponse(restAssuredContext, restAssuredInputBean, context,responseMap);
		resFormatter.handleCarryForward(restAssuredContext, restAssuredInputBean, context, responseMap);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}