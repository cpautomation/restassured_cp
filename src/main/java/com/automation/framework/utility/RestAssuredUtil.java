package com.automation.framework.utility;

import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.restassured3.RestAssuredRestDocumentation;
import org.springframework.restdocs.restassured3.RestDocumentationFilter;
import org.springframework.stereotype.Component;


import com.automation.framework.beans.RestAssuredBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.gson.Gson;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

@Component
public class RestAssuredUtil {
	private static Logger log = LogManager.getLogger(RestAssuredUtil.class.getName());

	@SuppressWarnings("unchecked")
	public RestAssuredBean prepareAssuredBean(Map<String, Object> map) {
		RestAssuredBean restAssuredBean = new RestAssuredBean();
		System.out.println("restAssuredBean inside");
		
		restAssuredBean.setUrl((String) map.get("url"));
		restAssuredBean.setContentType((ContentType) map.get("contentType"));
		restAssuredBean.setAcceptType((ContentType) map.get("acceptType"));
		restAssuredBean.setMethodType((String) map.get("methodType"));
		restAssuredBean.setHeaders((Map<String, String>) map.get("headers"));
		restAssuredBean.setRequestBody((Object) map.get("requestBean"));
		
		
		restAssuredBean.setCustomParam((String) map.get("customParam"));

		
		if(restAssuredBean.getMethodType().equals("GET") || restAssuredBean.getContentType().equals(ContentType.URLENC)) {
			ObjectMapper m = new ObjectMapper();
			restAssuredBean.setRequestParams(m.convertValue(map.get("requestBean"), Map.class));
		}
		return restAssuredBean;
	}
	
	
	
	public ValidatableResponse prepareAssured(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		ValidatableResponse response  = null;
		String methodType = restAssuredBean.getMethodType();
		System.out.println("before url check"+methodType +" "+restAssuredBean);
		if("POST".equalsIgnoreCase(methodType) && restAssuredBean.getContentType().equals(ContentType.URLENC)) {
			System.out.println("after url check");
			response = preparePostHeaderAssuredFormparms(spec,restAssuredBean);
		} else if("POST".equalsIgnoreCase(methodType) && restAssuredBean.isHeader() && (restAssuredBean.getCustomParam()!=null && restAssuredBean.getCustomParam().equals("WITHNAMINGSTRATEGY"))) {
			response = preparePostHeaderAssuredWithNamingStrategy(spec,restAssuredBean);
		} else if("POST".equalsIgnoreCase(methodType) && restAssuredBean.isHeader()) {
			response = preparePostHeaderAssured(spec,restAssuredBean);
		} else if("POST".equalsIgnoreCase(methodType) && !restAssuredBean.isHeader()) {
			response = preparePostAssured(spec,restAssuredBean);
		} 
		else {
			response = prepareGetWithHeader(spec,restAssuredBean);
		}
		
		return response;
	}
	
	private ValidatableResponse preparePostAssured(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		ValidatableResponse response = null;
		try {
			response = RestAssured
					.given(spec)
					.log()
					.all()
					.accept(restAssuredBean.getAcceptType())
					.with()
					.contentType(restAssuredBean.getContentType())
					.body(restAssuredBean.getRequestBody())
					
					.when()
					.post(restAssuredBean.getUrl())
					.then()
					.assertThat()
					.statusCode(restAssuredBean.getHttpExpectedCode());
			
			
			log.debug(restAssuredBean);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	
	private ValidatableResponse preparePostHeaderAssured(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		ValidatableResponse response = null;
		try {
		System.out.println("restAssuredBean.getUrl()"+restAssuredBean.getUrl()+restAssuredBean.getRequestBody());
		response = RestAssured
					.given(spec)
					.log()
					.all()
					.accept(restAssuredBean.getAcceptType())
					.with()
					.contentType(restAssuredBean.getContentType())
					.headers(restAssuredBean.getHeaders())
					.body(restAssuredBean.getRequestBody())
					
					.when()
					.post(restAssuredBean.getUrl())
					.then()
					.assertThat()
					.statusCode(restAssuredBean.getHttpExpectedCode());
			
		            log.debug(restAssuredBean);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	private ValidatableResponse preparePostHeaderAssuredWithNamingStrategy(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		ValidatableResponse response = null;
		try {
		String JSON=new Gson().toJson(restAssuredBean.getRequestBody());
		System.out.println("preparePostHeaderAssuredWithNamingStrategy.getUrl()"+restAssuredBean.getUrl()+restAssuredBean.getRequestBody()+JSON);
		response = RestAssured
					.given(spec)
					.log()
					.all()
					.accept(restAssuredBean.getAcceptType())
					.with()
					.contentType(restAssuredBean.getAcceptType())
					.headers(restAssuredBean.getHeaders())
					.body(JSON)
					
					.when()
					.post(restAssuredBean.getUrl())
					.then()
					.assertThat()
					.statusCode(restAssuredBean.getHttpExpectedCode());
			
		            log.debug(restAssuredBean);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	private ValidatableResponse prepareGetAssured(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		System.out.println("Inside prepareGetAssured");
		ValidatableResponse response = RestAssured
				.given(spec)
				.queryParams(restAssuredBean.getRequestParams())
				.log()
				.all()
				.accept(restAssuredBean.getAcceptType())
				.with()
				.contentType(restAssuredBean.getContentType())
				.when()
				.get(restAssuredBean.getUrl())
				.then()
				.assertThat()
				.statusCode(restAssuredBean.getHttpExpectedCode());
				RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        		log.debug(restAssuredBean);
 
		return response;
	}
	
	private ValidatableResponse prepareGetWithHeader(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		System.out.println("Inside prepareGetWithHeader");
		ValidatableResponse response = RestAssured
				.given(spec)
				.log()
				.all()
				.accept(restAssuredBean.getAcceptType())
				.with()
				.contentType(restAssuredBean.getContentType())
				.headers(restAssuredBean.getHeaders())
				.when()
				.get(restAssuredBean.getUrl())
				.then()
				.assertThat()
				.statusCode(restAssuredBean.getHttpExpectedCode());
				RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        		log.debug(restAssuredBean);
 
		return response;
	}
	

	private ValidatableResponse preparePostHeaderAssuredFormparms(RequestSpecification spec, RestAssuredBean restAssuredBean) {
		ValidatableResponse response = null;
		try {
			response = RestAssured
					.given(spec)
					.log()
					.all()
					.accept(restAssuredBean.getAcceptType())
					.with()
					.contentType(restAssuredBean.getContentType())
					.formParams(restAssuredBean.getRequestParams())
					
					.when()
					.post(restAssuredBean.getUrl())
					.then()
					.assertThat()
					.statusCode(restAssuredBean.getHttpExpectedCode());
			
		            log.debug(restAssuredBean);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
}