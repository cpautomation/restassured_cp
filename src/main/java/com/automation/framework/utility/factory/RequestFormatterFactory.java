package com.automation.framework.utility.factory;

import org.springframework.stereotype.Component;

import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.formatter.RequestDataFormatter;

@Component
public class RequestFormatterFactory {
	private final static String PACKAGE_NAME = "com.automation.framework.formatter.request.";

	public RequestDataFormatter getRequestDataFormatter(RestAssuredInputBean restAssuredInputBean) {
		String formatter = restAssuredInputBean.getReqFormatter();
		if (null == formatter || "".equalsIgnoreCase(formatter.trim())) {
			formatter = "DefaultRequestDataFormatter";
		}
		
		Class<?> c = null;
		try {
			c = Class.forName(PACKAGE_NAME + formatter);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		RequestDataFormatter requestDataFormatter = null;
		try {
			requestDataFormatter = (RequestDataFormatter) c.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return requestDataFormatter;
	}
}