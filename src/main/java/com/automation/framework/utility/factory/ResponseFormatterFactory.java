package com.automation.framework.utility.factory;

import org.springframework.stereotype.Component;

import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.formatter.ResponseDataFormatter;

@Component
public class ResponseFormatterFactory {
	private final static String PACKAGE_NAME = "com.automation.framework.formatter.response.";

	public ResponseDataFormatter getResponseDataFormatter(RestAssuredInputBean restAssuredInputBean) {
		
		String formatter = restAssuredInputBean.getResFormatter();
		if (null == formatter || "".equalsIgnoreCase(formatter.trim())) {
			formatter = "DefaultResponseDataFormatter";
		}
		Class<?> c = null;
		try {
			c = Class.forName(PACKAGE_NAME + formatter);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		ResponseDataFormatter responseDataFormatter = null;
		try {
			responseDataFormatter = (ResponseDataFormatter) c.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return responseDataFormatter;
	}
}