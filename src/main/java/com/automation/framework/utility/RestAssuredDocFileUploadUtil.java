package com.automation.framework.utility;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;
import com.automation.framework.beans.ParamsBean;
import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/***
 * @author Chandrakanth Nelge
 */
@Component
public class RestAssuredDocFileUploadUtil {
	private static Logger log = LogManager.getLogger(RestAssuredDocFileUploadUtil.class.getName());
	@Autowired
	private Environment env;
	
	public RestAssuredContext prepareRequestContext() {
		Map<String, String> contextMap = new HashMap<String, String>();
		
		Properties props = null;
		try {
			props = PropertiesLoaderUtils.loadProperties(new ClassPathResource("/" + env.getProperty("partner")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Set<Object> set = props.keySet();
		for (Object key : set) {
			String value = (String) props.get(key);
			contextMap.put(key.toString(), value.toString());
		}

		RestAssuredContext restAssuredContext = new RestAssuredContext();
		restAssuredContext.setContextMap(contextMap);

		return restAssuredContext;
	}

	/**
	 * Initiates the Loading property file and prepares the List of test scenario beans
	 */
	public RestAssuredInputBean init(String fileName, String useCase, RestAssuredContext context) {
		Properties props = null;
		try {
			props = loadPropertyFile(fileName);
		} catch (IOException e) {
			System.out.println(fileName + " LOADING IS FAILED");
			log.debug(fileName + " LOADING IS FAILED");
			e.printStackTrace();
		}

		RestAssuredInputBean restAssuredInputBean = prepareRestAssuredInputBean(useCase, props);
		restAssuredInputBean.setUrl(context.getContextMap().get(useCase + "-url"));
		validateTestScenario(restAssuredInputBean);

		return restAssuredInputBean;
	}

	/**
	 * Loads the property file
	 */
	private Properties loadPropertyFile(String fileName) throws IOException {
		System.out.println("Processing File = " + fileName + ".properties");
		log.debug("Processing File = " + fileName + ".properties");
		ClassPathResource resource = new ClassPathResource("/" + fileName + ".properties");
		Properties props = PropertiesLoaderUtils.loadProperties(resource);

		return props;
	}

	/**
	 * Prepare RestAssuredInputBean from property file against a test scenario
	 */
	private RestAssuredInputBean prepareRestAssuredInputBean(String useCase, Properties props) {
		useCase = useCase + "-";
		RestAssuredInputBean restAssuredInputBean = new RestAssuredInputBean();
		
		restAssuredInputBean.setContentType(props.getProperty(useCase + "req-contentType"));
		restAssuredInputBean.setAcceptType(props.getProperty(useCase + "req-acceptType"));
		restAssuredInputBean.setMethodType(props.getProperty(useCase + "req-methodType"));
		restAssuredInputBean.setReqBean(props.getProperty(useCase + "req-bean"));
		restAssuredInputBean.setResBean(props.getProperty(useCase + "res-bean"));
		if(props.getProperty(useCase + "req-customParam")!=null)
			restAssuredInputBean.setCustomParam(props.getProperty(useCase + "req-customParam"));
		if(null != props.getProperty(useCase + "req-formatter")) {
			restAssuredInputBean.setReqFormatter(props.getProperty(useCase + "req-formatter"));
		}
		if(null != props.getProperty(useCase + "res-formatter")) {
			restAssuredInputBean.setResFormatter(props.getProperty(useCase + "res-formatter"));
		}
		System.out.println(props.getProperty(useCase + "req-body"));
		log.debug(props.getProperty(useCase + "req-body"));
		if(props.getProperty(useCase + "req-methodType").equals("POST"))
		{
		restAssuredInputBean.setReqBody(loadPropertyFileFromResource(props.getProperty(useCase + "req-body")));
		}
		
		if (null != props.getProperty(useCase + "res-http-expected-code")) {
			int httpExpectedCode = Integer.parseInt(props.getProperty(useCase + "res-http-expected-code"));
			restAssuredInputBean.setExpectedCode(httpExpectedCode);
		}else {
			restAssuredInputBean.setExpectedCode(200);
		}
		
		if (null != props.getProperty(useCase + "res-expected-value")) {
			
			System.out.println("validation+++++:"+useCase+props.getProperty(useCase + "res-expected-value"));
			restAssuredInputBean.setExpectedValue(props.getProperty(useCase + "res-expected-value"));
			
		}
		
		if (null != props.getProperty(useCase + "req-header")) {
			restAssuredInputBean.setHeader(true);
		}
		
		restAssuredInputBean.setCarryForwardFields(props.getProperty(useCase + "res-carryforwrard"));
		
		if(null != props.getProperty(useCase + "req-body-params")) {
			String jsonReqParams = loadPropertyFileFromResource(props.getProperty(useCase + "req-body-params"));
			Type listType = new TypeToken<ArrayList<ParamsBean>>(){}.getType();
			List<ParamsBean> paramsBeans = new Gson().fromJson(jsonReqParams, listType);
			restAssuredInputBean.setReqBodyParams(paramsBeans);
		}
		
		restAssuredInputBean.setHeaderCarryForwardFields(props.getProperty(useCase + "header-carryforwrard"));

		System.out.println(restAssuredInputBean);
		log.debug(restAssuredInputBean);
		return restAssuredInputBean;
	}

	/**
	 * Validate the test scenario input
	 * @param restAssuredInputBean TODO Validate all fields here
	 */
	private void validateTestScenario(RestAssuredInputBean restAssuredInputBean) {
		if (null == restAssuredInputBean.getUrl()) {
			new Exception("Rest Service URL is not provided");
		}

		// TODO VALIDATE URL FORMAT
	}
	
	public String loadPropertyFileFromResource (String fileName) {
		//System.out.println("Loading Fileee = " + fileName);
		log.debug("Loading Fileee = " + fileName);
        String jsonStr = null;
		try {
			ClassLoader classLoader = RestAssuredDocFileUploadUtil.class.getClassLoader();
	        File file = new File(classLoader.getResource("templates/" + fileName).getFile());
			jsonStr = new String(Files.readAllBytes(file.toPath()));
        } catch (Exception e) {
            //System.out.println("Could not read json file  = " + fileName);
            log.debug("Could not read json file  = " + fileName);
            e.printStackTrace();
        }
		
		return jsonStr;
	}

}