package com.automation.framework.utility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.ITestContext;

import com.automation.framework.beans.ParamsBean;
import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.beans.request.RequestBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.google.gson.Gson;

public class RequestFormatterHelper {
	
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final Gson GSON = new Gson();
	private static final String PACKAGE_NAME = "com.automation.framework.beans.";
	
	private static Class<?> getRequestBeanClass(String reqBean) {
		Class<?> c = null;
		try {
			c = Class.forName(PACKAGE_NAME + reqBean);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("INVALID REQUEST BEAN, BEAN NOT FOUND", e);
		}
		
		return c;
	}
	
	@SuppressWarnings("unchecked")
	public static RequestBean prepareRequestBean(RestAssuredInputBean inputBean,String json, Map<String, String> properties, String reqBean, ITestContext context) {
		Map<String, Object> map = (HashMap<String, Object>) GSON.fromJson(json, HashMap.class);
		for(Map.Entry<String, Object> entry : map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if(value instanceof Map) {
				deepMerge(inputBean, (Map<String, Object>)value, properties, context, key);
			} else {
				List<ParamsBean> paramsBeans = inputBean.getReqBodyParams();
				for(ParamsBean paramsBean : paramsBeans) {
					if(paramsBean.getName().equals(key)) {
						map.put(key, paramsBean.getValue());
						break;
					}
				}
				
				if(properties.containsKey(key)) {
					map.put(key, properties.get(key));
				}
				
				if(null != context.getAttribute(key)) {
					map.put(key, context.getAttribute(key));
				}
			}
		}
		
		Class<?> clazz = getRequestBeanClass(reqBean);
		System.out.println("prepareRequestBean:"+map+"     class name :"+ clazz);
		Object object = OBJECT_MAPPER.convertValue(map, clazz);
		//OBJECT_MAPPER.setPropertyNamingStrategy(new PropertyNamingStrategy.UpperCamelCaseStrategy());
		if(false == (object instanceof RequestBean)) {
			throw new RuntimeException("INVALID REQUEST BEAN DEFINITION");
		}
		return (RequestBean)object;
	}
	public static RequestBean prepareRequestBeanWithNamingStrategy(RestAssuredInputBean inputBean,String json, Map<String, String> properties, String reqBean, ITestContext context, UpperCamelCaseStrategy upperCamelCaseStrategy) {
		final ObjectMapper OBJECT_MAPPER_WITH_STRATEGY = new ObjectMapper();
		OBJECT_MAPPER_WITH_STRATEGY.setPropertyNamingStrategy(upperCamelCaseStrategy);
		Map<String, Object> map = (HashMap<String, Object>) GSON.fromJson(json, HashMap.class);
		for(Map.Entry<String, Object> entry : map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if(value instanceof Map) {
				deepMerge(inputBean, (Map<String, Object>)value, properties, context, key);
			} else {
				List<ParamsBean> paramsBeans = inputBean.getReqBodyParams();
				for(ParamsBean paramsBean : paramsBeans) {
					if(paramsBean.getName().equals(key)) {
						map.put(key, paramsBean.getValue());
						break;
					}
				}
				
				if(properties.containsKey(key)) {
					map.put(key, properties.get(key));
				}
				
				if(null != context.getAttribute(key)) {
					map.put(key, context.getAttribute(key));
				}
			}
		}
		
		Class<?> clazz = getRequestBeanClass(reqBean);
		System.out.println("prepareRequestBean:"+map+"     class name :"+ clazz);
		Object object = OBJECT_MAPPER_WITH_STRATEGY.convertValue(map, clazz);
		//OBJECT_MAPPER.setPropertyNamingStrategy(new PropertyNamingStrategy.UpperCamelCaseStrategy());
		if(false == (object instanceof RequestBean)) {
			throw new RuntimeException("INVALID REQUEST BEAN DEFINITION");
		}
		return (RequestBean)object;
	}
	
	@SuppressWarnings("unchecked")
	private static void deepMerge(RestAssuredInputBean inputBean, Map<String, Object> map, Map<String, String> properties, ITestContext context, String parentKey) {
		for(Map.Entry<String, Object> entry: map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if(value instanceof Map) {
				deepMerge(inputBean, (Map<String, Object>)value, properties, context, parentKey+"."+key);
			} else {
				List<ParamsBean> paramsBeans = inputBean.getReqBodyParams();
				for(ParamsBean paramsBean : paramsBeans) {
					if(paramsBean.getName().equals((parentKey+"."+key))) {
						map.put(key, paramsBean.getValue());
						break;
					}
				}
				
				if(properties.containsKey(parentKey+"."+key)) {
					map.put(key, properties.get(parentKey+"."+key));
				}
				
				if(null != context.getAttribute(key)) {
					map.put(key, context.getAttribute(parentKey+"."+key));
				}
			}
		}
	}

}