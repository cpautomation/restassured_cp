package com.automation.framework.db;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class DbTestRepository {
	
	
	@Autowired
	private JdbcTemplate messagingJdbcTemplate;
	
	@Autowired
	private JdbcTemplate ecrJdbcTemplate;
	
	@Autowired
	private JdbcTemplate pokerJdbcTemplate;
	
	@Autowired
	private JdbcTemplate trnyJdbcTemplate;
	
	@Autowired
	private JdbcTemplate fundJdbcTemplate;
	
	@Autowired
	private JdbcTemplate riskJdbcTemplate;
	
	@Autowired
	private JdbcTemplate cashierJdbcTemplate;
	
	@Autowired
	private JdbcTemplate promoJdbcTemplate;
	
	@Autowired
	private JdbcTemplate backofficeJdbcTemplate;
	
	@Autowired
	private JdbcTemplate schedulerJdbcTemplate;
	
	@Autowired
	private JdbcTemplate vendorJdbcTemplate;
	
	@Autowired
	private JdbcTemplate loyaltyJdbcTemplate;
	
	@Autowired
	private JdbcTemplate playpokerJdbcTemplate;
	
	@Autowired
	private JdbcTemplate casinoJdbcTemplate;
	
	@Autowired
	private JdbcTemplate retailJdbcTemplate;
	
	@Autowired
	private JdbcTemplate bireportsJdbcTemplate;
	
	@Autowired
	private JdbcTemplate bingobuckJdbcTemplate;
	
	@Autowired
	private JdbcTemplate bingoJdbcTemplate;
	
	@Autowired
	private JdbcTemplate segmentJdbcTemplate;
	
	@Autowired
	private JdbcTemplate lotteryJdbcTemplate;
	
	
	/* Use the respective template to query corresponding schema in database
	 * For example messaging schema use messagingJdbcTemplate
	 */
	public String getTestData(String extEcrId)
	{
		
		
		String query = "select c_ecr_id from tbl_ecr where c_external_id="+extEcrId;
		String ecrId =  ecrJdbcTemplate.queryForList(query, String.class).get(0);
		
		
		return ecrId;
	}

}
