package com.automation.framework.db;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class DBConfig {

	@Bean(name="messagingDb")
	@Primary
	@ConfigurationProperties(prefix="db.messaging")
	public DataSource messagingDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "messagingJdbcTemplate")
	public JdbcTemplate messagingJdbcTemplate(@Qualifier("messagingDb") DataSource messagingDataSource) {
		return new JdbcTemplate(messagingDataSource);
	}

	@Bean(name="ecrDb")
	@ConfigurationProperties(prefix="db.ecr")
	public DataSource ecrDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "ecrJdbcTemplate")
	public JdbcTemplate ecrJdbcTemplate(@Qualifier("ecrDb") DataSource ecrDataSource) {
		return new JdbcTemplate(ecrDataSource);
	}
	
	@Bean(name="pokerDb")
	@ConfigurationProperties(prefix="db.poker")
	public DataSource pokerDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "pokerJdbcTemplate")
	public JdbcTemplate pokerJdbcTemplate(@Qualifier("pokerDb") DataSource pokerDataSource) {
		return new JdbcTemplate(pokerDataSource);
	}
	
	@Bean(name="trnyDb")
	@ConfigurationProperties(prefix="db.trny")
	public DataSource trnyDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "trnyJdbcTemplate")
	public JdbcTemplate trnyJdbcTemplate(@Qualifier("trnyDb") DataSource trnyDataSource) {
		return new JdbcTemplate(trnyDataSource);
	}
	
	@Bean(name="fundDb")
	@ConfigurationProperties(prefix="db.fund")
	public DataSource fundDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "fundJdbcTemplate")
	public JdbcTemplate fundJdbcTemplate(@Qualifier("fundDb") DataSource fundDataSource) {
		return new JdbcTemplate(fundDataSource);
	}
	
	@Bean(name="riskDb")
	@ConfigurationProperties(prefix="db.risk")
	public DataSource riskDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "riskJdbcTemplate")
	public JdbcTemplate riskJdbcTemplate(@Qualifier("riskDb") DataSource riskDataSource) {
		return new JdbcTemplate(riskDataSource);
	}
	
	@Bean(name="cashierDb")
	@ConfigurationProperties(prefix="db.cashier")
	public DataSource cashierDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "cashierJdbcTemplate")
	public JdbcTemplate cashierJdbcTemplate(@Qualifier("cashierDb") DataSource cashierDataSource) {
		return new JdbcTemplate(cashierDataSource);
	}
	
	@Bean(name="promoDb")
	@ConfigurationProperties(prefix="db.promo")
	public DataSource promoDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "promoJdbcTemplate")
	public JdbcTemplate promoJdbcTemplate(@Qualifier("promoDb") DataSource promoDataSource) {
		return new JdbcTemplate(promoDataSource);
	}
	
	@Bean(name="backofficeDb")
	@ConfigurationProperties(prefix="db.backoffice")
	public DataSource backofficeDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "backofficeJdbcTemplate")
	public JdbcTemplate backofficeJdbcTemplate(@Qualifier("backofficeDb") DataSource backofficeDataSource) {
		return new JdbcTemplate(backofficeDataSource);
	}
	
	@Bean(name="schedulerDb")
	@ConfigurationProperties(prefix="db.scheduler")
	public DataSource schedulerDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "schedulerJdbcTemplate")
	public JdbcTemplate schedulerJdbcTemplate(@Qualifier("schedulerDb") DataSource schedulerDataSource) {
		return new JdbcTemplate(schedulerDataSource);
	}
	
	@Bean(name="vendorDb")
	@ConfigurationProperties(prefix="db.vendor")
	public DataSource vendorDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "vendorJdbcTemplate")
	public JdbcTemplate vendorJdbcTemplate(@Qualifier("vendorDb") DataSource vendorDataSource) {
		return new JdbcTemplate(vendorDataSource);
	}
	
	@Bean(name="loyaltyDb")
	@ConfigurationProperties(prefix="db.loyalty")
	public DataSource loyaltyDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "loyaltyJdbcTemplate")
	public JdbcTemplate loyaltyJdbcTemplate(@Qualifier("loyaltyDb") DataSource loyaltyDataSource) {
		return new JdbcTemplate(loyaltyDataSource);
	}
	
	@Bean(name="playpokerDb")
	@ConfigurationProperties(prefix="db.playpoker")
	public DataSource playpokerDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "playpokerJdbcTemplate")
	public JdbcTemplate playpokerJdbcTemplate(@Qualifier("playpokerDb") DataSource playpokerDataSource) {
		return new JdbcTemplate(playpokerDataSource);
	}
	
	@Bean(name="casinoDb")
	@ConfigurationProperties(prefix="db.casino")
	public DataSource casinoDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "casinoJdbcTemplate")
	public JdbcTemplate casinoJdbcTemplate(@Qualifier("casinoDb") DataSource casinoDataSource) {
		return new JdbcTemplate(casinoDataSource);
	}
	
	@Bean(name="retailDb")
	@ConfigurationProperties(prefix="db.retail")
	public DataSource retailDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "retailJdbcTemplate")
	public JdbcTemplate retailJdbcTemplate(@Qualifier("retailDb") DataSource retailDataSource) {
		return new JdbcTemplate(retailDataSource);
	}
	
	@Bean(name="bireportsDb")
	@ConfigurationProperties(prefix="db.bireports")
	public DataSource bireportsDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "bireportsJdbcTemplate")
	public JdbcTemplate bireportsJdbcTemplate(@Qualifier("bireportsDb") DataSource bireportsDataSource) {
		return new JdbcTemplate(bireportsDataSource);
	}
	
	
	@Bean(name="bingobuckDb")
	@ConfigurationProperties(prefix="db.bingobuck")
	public DataSource bingobuckDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "bingobuckJdbcTemplate")
	public JdbcTemplate bingobuckJdbcTemplate(@Qualifier("bingobuckDb") DataSource bingobuckDataSource) {
		return new JdbcTemplate(bingobuckDataSource);
	}
	
	@Bean(name="bingoDb")
	@ConfigurationProperties(prefix="db.bingo")
	public DataSource bingoDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "bingoJdbcTemplate")
	public JdbcTemplate bingoJdbcTemplate(@Qualifier("bingoDb") DataSource bingoDataSource) {
		return new JdbcTemplate(bingoDataSource);
	}
	
	@Bean(name="segmentDb")
	@ConfigurationProperties(prefix="db.segment")
	public DataSource segmentDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "segmentJdbcTemplate")
	public JdbcTemplate segmentJdbcTemplate(@Qualifier("segmentDb") DataSource segmentDataSource) {
		return new JdbcTemplate(segmentDataSource);
	}
	
	@Bean(name="lotteryDb")
	@ConfigurationProperties(prefix="db.lottery")
	public DataSource lotteryDataSource() {
	    return DataSourceBuilder.create().build();
	}
	
	@Bean(name = "lotteryJdbcTemplate")
	public JdbcTemplate lotteryJdbcTemplate(@Qualifier("lotteryDb") DataSource lotteryDataSource) {
		return new JdbcTemplate(lotteryDataSource);
	}
}
