package com.automation.framework.formatter.request;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.ParameterDescriptor;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.springframework.restdocs.request.RequestDocumentation;
import org.springframework.stereotype.Component;
import org.testng.ITestContext;

import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;

import com.automation.framework.beans.request.RequestBean;
import com.automation.framework.formatter.RequestDataFormatter;
import com.automation.framework.utility.RequestFormatterHelper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.restassured.http.ContentType;
import static org.springframework.restdocs.snippet.Attributes.key;

@Component
public class DefaultRequestDataFormatter implements RequestDataFormatter {
	private static Logger log = LogManager.getLogger(DefaultRequestDataFormatter.class.getName());
	
	@Override
	public String prepareUrl(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean) {
		
		return restAssuredInputBean.getUrl();
	}
	
	
	@Override
	public String prepareCustomParam(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean) {
		return restAssuredInputBean.getCustomParam();
	}
	
	@Override
	public ContentType prepareContentType(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean) {
		ContentType contentType = ContentType.JSON;
		switch(restAssuredInputBean.getContentType().trim().toUpperCase()) {
			case "JSON":
				contentType = ContentType.JSON;
				break;
			case "XML":
				contentType = ContentType.XML;
				break;
			case "TEXT":
				contentType = ContentType.TEXT;
				break;
			case "URLENC":
				contentType = ContentType.URLENC;
				break;
		}
		
		return contentType;
	}

	@Override
	public ContentType prepareAcceptType(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean) {
		ContentType acceptType = ContentType.JSON;
		switch(restAssuredInputBean.getAcceptType().trim().toUpperCase()) {
			case "JSON":
				acceptType = ContentType.JSON;
				break;
			case "XML":
				acceptType = ContentType.XML;
				break;
			case "TEXT":
				acceptType = ContentType.TEXT;
				break;
		}
		
		return acceptType;
	}

	@Override
	public String prepareMethodType(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean) {
		String methodType = "POST";
		switch(restAssuredInputBean.getMethodType().trim().toUpperCase()) {
			case "POST":
				methodType = "POST";
				break;
			case "GET":
				methodType = "GET";
				break;
			case "PATCH":
				methodType = "PATCH";
				break;
			case "PUT":
				methodType = "PUT";
				break;
		}
		
		return methodType;
	}
	
	@Override
	public Map<String, Object> prepareRequestHeaders(RestAssuredContext restAssuredContext,RestAssuredInputBean restAssuredInputBean, ITestContext context) {
		return null;
	}
	
	

	@Override
	public RequestBean prepareRequestBody(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context) {
		
		String reqJson = restAssuredInputBean.getReqBody();
		
		Map<String, String> propertyMap = restAssuredContext.getContextMap();
		
		RequestBean requestBean = RequestFormatterHelper.prepareRequestBean(restAssuredInputBean, reqJson, propertyMap, restAssuredInputBean.getReqBean(),context);
		
		return requestBean;
	}
	@Override
	public RequestBean prepareRequestBodyWithNamingStrategy(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context, UpperCamelCaseStrategy upperCamelCaseStrategy) {
		String reqJson = restAssuredInputBean.getReqBody();
		
		Map<String, String> propertyMap = restAssuredContext.getContextMap();
		
		RequestBean requestBean = RequestFormatterHelper.prepareRequestBeanWithNamingStrategy(restAssuredInputBean, reqJson, propertyMap, restAssuredInputBean.getReqBean(),context,upperCamelCaseStrategy);
		
		return requestBean;
	}
	/**
	 * HELPER METHOD
	 */
	
	

	/**
	 * HELPER METHOD
	 */
	
	
	
}