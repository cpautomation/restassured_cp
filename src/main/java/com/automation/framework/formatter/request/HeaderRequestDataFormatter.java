package com.automation.framework.formatter.request;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.testng.ITestContext;
import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;

@Component
public class HeaderRequestDataFormatter extends DefaultRequestDataFormatter {
	
	@Override
	public Map<String, Object> prepareRequestHeaders(RestAssuredContext restAssuredContext,
			RestAssuredInputBean restAssuredInputBean, ITestContext context) {
		Map<String, Object> headers = new HashMap<String, Object>();
		System.out.println("Authorization DLB"+context.getAttribute("Authorization").toString());
		headers.put("Authorization", context.getAttribute("Authorization").toString());
        return headers;
	}
}

