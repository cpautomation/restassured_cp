package com.automation.framework.formatter.request;

import java.nio.file.Paths;

public class FileConstants {

	public static String getprojectvalues()
	{
		return System.getProperty("user.dir");
	}
	
	public static String resolveFilePath(String uri) {
		return Paths.get(uri).toString();
	}
}
