package com.automation.framework.formatter.request;

import java.util.Map;

import org.springframework.stereotype.Component;
import org.testng.ITestContext;


import com.automation.framework.beans.Authorize;
import com.automation.framework.beans.ClientDetails;

import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.beans.request.RequestBean;
import com.google.gson.Gson;

@Component
public class AuthorizeRequestDataFormatter extends DefaultRequestDataFormatter {
	@Override
	public RequestBean prepareRequestBody(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context) {
		return getRequestBody(restAssuredContext, restAssuredInputBean, context);
	}

	private RequestBean getRequestBody(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context) 
	{
		Authorize auth = (Authorize)super.prepareRequestBody(restAssuredContext, restAssuredInputBean, context);
		String reqJson = restAssuredInputBean.getReqBody();
		Map<String, String> contextMap = restAssuredContext.getContextMap();
		Gson gson = new Gson();
		
		auth = (Authorize) gson.fromJson(reqJson, Authorize.class);
		ClientDetails clientDetails = auth.getClientDetails();
		clientDetails.setPartnerID(contextMap.get("partnerID"));
		clientDetails.setSecretCode(contextMap.get("secretCode"));
		auth.setClientDetails(clientDetails);
		
		return auth;
	}
}