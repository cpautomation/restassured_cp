package com.automation.framework.formatter.response;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.ITestContext;
import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.formatter.request.FileConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
public class HeaderResponseDataFormatter extends DefaultResponseDataFormatter {

@Override
public void handleCarryForward(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean,
ITestContext context, Map<String, Object> responseMap)  {
	System.out.println("Entering OpenGameResponceFormatter21 response");
	System.out.println("restAssuredInputBean.getCarryForwardFields()");
if (null == restAssuredInputBean.getCarryForwardFields()
|| "".equals(restAssuredInputBean.getCarryForwardFields().trim())) {
return;
}
System.out.println("responseMap:"+responseMap);
String ecrExternalID = (String) responseMap.get("ecrExternalID");

InputStream input=null;
FileOutputStream out=null;
try {
	//FileInputStream in = new FileInputStream("C:\\Users\\lavanya\\Documents\\RestAssured\\rest-assured\\src\\main\\resources\\config.properties");
	System.out.println(FileConstants.getprojectvalues());
	String filePath = FileConstants.resolveFilePath("/src/main/resources/config.properties");
	String fullPath = FileConstants.getprojectvalues() + filePath;
	input = new FileInputStream(fullPath);
	Properties props = new Properties();
	props.load(input);
	input.close();
	
	System.out.println(FileConstants.getprojectvalues());
	String filePath1 = FileConstants.resolveFilePath("/src/main/resources/config.properties");
	String fullPath1 = FileConstants.getprojectvalues() + filePath1;
	out = new FileOutputStream(fullPath1);
	//FileOutputStream out = new FileOutputStream("C:\\Users\\lavanya\\Documents\\RestAssured\\rest-assured\\src\\main\\resources\\config.properties");
	props.setProperty("OldecrExternalID",ecrExternalID);
	props.setProperty("betagouserId",ecrExternalID);
	props.store(out, null);
	out.close();
} catch (FileNotFoundException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
} 

}
}
