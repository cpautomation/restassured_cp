package com.automation.framework.formatter.response;
import java.util.HashMap;
import java.util.Map;

import org.testng.ITestContext;

import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
public class OtpReferenceResponseDataFormatter extends DefaultResponseDataFormatter {

 

    @Override
    public void handleCarryForward(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean,
    ITestContext context, Map<String, Object> responseMap) {
    if (null == restAssuredInputBean.getCarryForwardFields()
    || "".equals(restAssuredInputBean.getCarryForwardFields().trim())) {
    return;
    }
    HashMap<String,String> mapping = (HashMap<String,String>) responseMap.get("response");
    String otpReference = mapping.get("otpReference");
    restAssuredContext.getResponserMap().put("otpReference", otpReference);
    System.out.println("Test user1"+otpReference);
    context.setAttribute("otpReference", otpReference);
    System.out.println("Test user"+context.getAttribute("otpReference"));
    }
}
