package com.automation.framework.formatter.response;

import static org.testng.Assert.assertTrue;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.testng.ITestContext;


import com.automation.framework.beans.ParamsBean;
import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.formatter.ResponseDataFormatter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import io.restassured.path.json.JsonPath;
import io.restassured.response.ValidatableResponse;

@Component
public class DefaultResponseDataFormatter implements ResponseDataFormatter {
	private static Logger log = LogManager.getLogger(DefaultResponseDataFormatter.class.getName());
	
	@Override
	public Map<String, Object> handleResponse(RestAssuredContext restAssuredContext, RestAssuredInputBean inputBean, ValidatableResponse response) {
		JsonPath jsonPath = response.extract().jsonPath();
		Map<String, Object> map = jsonPath.get();
		
        
        
		if(StringUtils.isNotEmpty(inputBean.getHeaderCarryForwardFields())) {
			for(String header : inputBean.getHeaderCarryForwardFields().split(",")) {
				map.put(header, response.extract().headers().getValue(header));
			}
		}
		
		return map;
	}
	
	@Override
	public void validateResponse(RestAssuredContext restAssuredContext, RestAssuredInputBean inputBean, ITestContext context, Map<String, Object> responseMap) {
		if(null == inputBean.getExpectedValue() || "".equals(inputBean.getExpectedValue().trim())) {
			assertTrue(true);
		}
		System.out.println("inputBean.getExpectedValue():"+inputBean.getExpectedValue());
		Type listType = new TypeToken<ArrayList<ParamsBean>>(){}.getType();
		
		List<ParamsBean> expectedValuesBeans = new Gson().fromJson(inputBean.getExpectedValue(), listType);
		boolean isValidResponse = true;
		System.out.println("responseMapresponseMap:"+responseMap);
		for(ParamsBean expectedValueBean : expectedValuesBeans) {
			System.out.println("expectedValue:"+expectedValueBean);
			if(expectedValueBean.getParentName()!=null) {
				responseMap.putAll((Map<String, Object>) responseMap.get(expectedValueBean.getParentName()));
			}
			
			if(responseMap.containsKey(expectedValueBean.getName())) {
				String returnValue = responseMap.get(expectedValueBean.getName()).toString();
				if(isValidResponse && returnValue.equalsIgnoreCase(expectedValueBean.getValue())) {
					isValidResponse = true;
				}
				else {
					isValidResponse = false;
					break;
				}
			} else {
				isValidResponse = false;
				break;
			}
		}
		
		if(isValidResponse) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		
	}

	@Override
	public void handleCarryForward(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context, Map<String, Object> responseMap) {
		List<String> carryForwardFieldsList=null;
		if(null == restAssuredInputBean.getHeaderCarryForwardFields() || "".equals(restAssuredInputBean.getHeaderCarryForwardFields().trim())) {
			return;
		}
		else if(StringUtils.isNotEmpty(restAssuredInputBean.getHeaderCarryForwardFields()))
		{
		carryForwardFieldsList = Arrays.asList(restAssuredInputBean.getHeaderCarryForwardFields().split(","));
		}
		else
		{
			carryForwardFieldsList = Arrays.asList(restAssuredInputBean.getCarryForwardFields().split(","));	
		}
		log.debug("carryForwardFieldsList:"+carryForwardFieldsList+"responseMap:"+responseMap);
	System.out.println("carryForwardFieldsList:"+carryForwardFieldsList+"responseMap:"+responseMap);
		for(String carryForwardField : carryForwardFieldsList) {
		    String value =null;
		    value = responseMap.get(carryForwardField).toString();
			restAssuredContext.getResponserMap().put(carryForwardField, value);
			context.setAttribute(carryForwardField, value);
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void prepareCarryForward(RestAssuredContext restAssuredContext, Map<String, Object> resMap, String carryForwardFieldName) {
		for(Map.Entry<String, Object> entry : resMap.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if(value instanceof Map) {
				Map<String, Object> valueMap = (Map<String, Object>)value;
				deepMerge(restAssuredContext, valueMap, carryForwardFieldName);
			} else {
				restAssuredContext.getResponserMap().put(carryForwardFieldName, value.toString());
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private static void deepMerge(RestAssuredContext restAssuredContext, Map<String, Object> map, String parentKey) {
		for(Map.Entry<String, Object> entry: map.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			if(value instanceof Map) {
				deepMerge(restAssuredContext, (Map<String, Object>)value, parentKey+"."+key);
			} else {
				restAssuredContext.getResponserMap().put(parentKey, value.toString());
			}
		}
	}

}