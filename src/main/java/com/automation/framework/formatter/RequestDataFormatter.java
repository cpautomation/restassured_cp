package com.automation.framework.formatter;

import java.util.Map;

import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.PathParametersSnippet;
import org.testng.ITestContext;

import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;
import com.automation.framework.beans.request.RequestBean;
import com.fasterxml.jackson.databind.PropertyNamingStrategy.UpperCamelCaseStrategy;

import io.restassured.http.ContentType;

public interface RequestDataFormatter {
	String prepareUrl(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean);
	
	ContentType prepareContentType(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean);
	ContentType prepareAcceptType(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean);
	String prepareMethodType(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean);
	Map<String, Object> prepareRequestHeaders(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context);
	
	RequestBean prepareRequestBody(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context);
	RequestBean prepareRequestBodyWithNamingStrategy(RestAssuredContext restAssuredContext,
			RestAssuredInputBean restAssuredInputBean, ITestContext context,
			UpperCamelCaseStrategy upperCamelCaseStrategy);
	String prepareCustomParam(RestAssuredContext context, RestAssuredInputBean restAssuredInputBean);
	
}