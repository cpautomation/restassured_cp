package com.automation.framework.formatter;

import java.util.Map;

import org.testng.ITestContext;

import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;

import io.restassured.response.ValidatableResponse;

public interface ResponseDataFormatter {
	public Map<String, Object> handleResponse(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ValidatableResponse response);
	public void validateResponse(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context, Map<String, Object> responseMap);
	public void handleCarryForward(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean, ITestContext context, Map<String, Object> responseMap);
}