package com.automation.framework;

import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration;

import java.lang.reflect.Method;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.ManualRestDocumentation;
import org.springframework.restdocs.mustache.Mustache;
import org.springframework.restdocs.templates.StandardTemplateResourceResolver;
import org.springframework.restdocs.templates.TemplateFormats;
import org.springframework.restdocs.templates.mustache.MustacheTemplateEngine;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestNgMethods extends AbstractTestNGSpringContextTests {
	
	public RequestSpecification spec;
	public final ManualRestDocumentation restDocumentation = new ManualRestDocumentation();
	MustacheTemplateEngine templateEngine = new MustacheTemplateEngine(
											new StandardTemplateResourceResolver(TemplateFormats.asciidoctor()),Mustache.compiler()
											.escapeHTML(false).defaultValue(""));
	@BeforeMethod
	public void setUp(Method method) {
		spec = new RequestSpecBuilder().addFilter(documentationConfiguration(restDocumentation).templateEngine(templateEngine)).build();
		restDocumentation.beforeTest(getClass(), method.getName());
	}

	@AfterMethod
	public void tearDown() {
		this.restDocumentation.afterTest();
	}
}