package com.automation.framework;

import java.util.ArrayList;
import java.util.List;

import org.testng.ITestNGListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlSuite.ParallelMode;

import com.automation.framework.test.CreateOtpTest;
import com.automation.framework.test.DoLoginTest;
import com.automation.framework.test.verifyOtpTest;

public class AutomationApplicationTests {

	@Test
	public void beginTest() {
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] {
				DoLoginTest.class,
				CreateOtpTest.class,
				verifyOtpTest.class,
//				DoPayment.class,
				
				
		});
		testng.setParallel(ParallelMode.NONE);
		testng.setPreserveOrder(true);
		List<Class<? extends ITestNGListener>> classes = new ArrayList<Class<? extends ITestNGListener>>();
		classes.add(TestListenerAdapter.class);
		testng.setListenerClasses(classes);
		testng.run();
	}
}