package com.automation.framework.beans;

import com.automation.framework.beans.request.RequestBean;

public class DoLoginBean extends RequestBean {
	private String userName;
	private String password;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}