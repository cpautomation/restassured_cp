package com.automation.framework.beans;

import com.automation.framework.beans.request.RequestBean;

public class VerifyOTP extends RequestBean {
	
	private String otpReference;
	private String pin;
	private String phoneOTPSourceEnum;
	public String getOtpReference() {
		return otpReference;
	}
	public void setOtpReference(String otpReference) {
		this.otpReference = otpReference;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getPhoneOTPSourceEnum() {
		return phoneOTPSourceEnum;
	}
	public void setPhoneOTPSourceEnum(String phoneOTPSourceEnum) {
		this.phoneOTPSourceEnum = phoneOTPSourceEnum;
	}
	
	
	
}
