package com.automation.framework.beans;

public class DoPayment {
	private String debitorIban;
	private String email;
	private String partyId;
	private String bankCode;
	private String bankName;
	private String bankAddress;
	private String accountNumber;
	private String beneficiaryName;
	private String paymentAmount;
	private String paymentCurrency;
	private String feeAccountNumber;
	private String feeAccountCurrency;
	private String unStructurePayment;
	private String unStructurePaymentDetails;
	
	public String getDebitorIban() {
		return debitorIban;
	}
	public void setDebitorIban(String debitorIban) {
		this.debitorIban = debitorIban;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPartyId() {
		return partyId;
	}
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAddress() {
		return bankAddress;
	}
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	public String getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentCurrency() {
		return paymentCurrency;
	}
	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}
	public String getFeeAccountNumber() {
		return feeAccountNumber;
	}
	public void setFeeAccountNumber(String feeAccountNumber) {
		this.feeAccountNumber = feeAccountNumber;
	}
	public String getFeeAccountCurrency() {
		return feeAccountCurrency;
	}
	public void setFeeAccountCurrency(String feeAccountCurrency) {
		this.feeAccountCurrency = feeAccountCurrency;
	}
	public String getUnStructurePayment() {
		return unStructurePayment;
	}
	public void setUnStructurePayment(String unStructurePayment) {
		this.unStructurePayment = unStructurePayment;
	}
	public String getUnStructurePaymentDetails() {
		return unStructurePaymentDetails;
	}
	public void setUnStructurePaymentDetails(String unStructurePaymentDetails) {
		this.unStructurePaymentDetails = unStructurePaymentDetails;
	}
}
