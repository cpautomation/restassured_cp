package com.automation.framework;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.automation.framework.utility.RestAssuredDocHelper;

public class CheckUserPhoneAvailablityTest extends TestNgMethods {

	@Autowired
	private RestAssuredDocHelper restAssuredDocHelper;

	@Test(enabled = true)
	public void checkUserPhoneAvailablity(ITestContext context) {
		System.out.println("authorize success test case executing.....");

		String fileName = "checkUserPhoneAvailablity";
		String useCase = "checkUserPhoneAvailablity";

		restAssuredDocHelper.doProcess(fileName, useCase, spec, context);
	}
	
	
}