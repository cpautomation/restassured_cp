package com.automation.framework;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.automation.framework.utility.RestAssuredDocHelper;

public class AuthorizeTest extends TestNgMethods {

	@Autowired
	private RestAssuredDocHelper restAssuredDocHelper;

	@Test(enabled = true)
	public void authorize(ITestContext context) {
		System.out.println("authorize success test case executing.....");

		String fileName = "authorize";
		String useCase = "authorize";

		restAssuredDocHelper.doProcess(fileName, useCase, spec, context);
	}
	
	@Test(enabled = false)
	public void authorizeFail(ITestContext context) {
		System.out.println("authorize fail test case executing.....");

		String fileName = "authorize";
		String useCase = "authorizefail";

		restAssuredDocHelper.doProcess(fileName, useCase, spec, context);
	}
}