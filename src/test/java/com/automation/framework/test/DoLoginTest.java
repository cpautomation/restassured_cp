package com.automation.framework.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.automation.framework.TestNgMethods;
import com.automation.framework.utility.RestAssuredDocHelper;

import io.restassured.response.ValidatableResponse;

public class DoLoginTest extends TestNgMethods {

	@Autowired
	private RestAssuredDocHelper restAssuredDocHelper;

	@Test(priority=1)
	public void doLogin(ITestContext context) {
		System.out.println("doLogin success test case executing.....");
		
		String fileName = "doLogin";
		String useCase = "doLogin";

		restAssuredDocHelper.doProcess(fileName, useCase, spec, context);
		
	}
	
	
}