package com.automation.framework.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.automation.framework.TestNgMethods;
import com.automation.framework.utility.RestAssuredDocHelper;

import io.restassured.response.ValidatableResponse;

public class verifyOtpTest extends TestNgMethods {

	@Autowired
	private RestAssuredDocHelper restAssuredDocHelper;

	@Test(priority=2)
	public void verifyOtp(ITestContext context) {
		System.out.println("verifyOtp success test case executing.....");
		
		String fileName = "verifyOtp";
		String useCase = "verifyOtp";

		restAssuredDocHelper.doProcess(fileName, useCase, spec, context);
		
	}
	
	
}