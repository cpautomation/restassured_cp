package com.automation.response.formatter;


import java.util.List;
import java.util.Map;


import org.testng.ITestContext;
import com.automation.framework.beans.RestAssuredContext;
import com.automation.framework.beans.RestAssuredInputBean;

import com.automation.framework.formatter.response.DefaultResponseDataFormatter;
import com.fasterxml.jackson.databind.ObjectMapper;
public class OtpReferenceResponseDataFormatter extends DefaultResponseDataFormatter {

	@Override
	public void handleCarryForward(RestAssuredContext restAssuredContext, RestAssuredInputBean restAssuredInputBean,
	ITestContext context, Map<String, Object> responseMap) {
	if (null == restAssuredInputBean.getCarryForwardFields()
	|| "".equals(restAssuredInputBean.getCarryForwardFields().trim())) {
	return;
	}
	List<Object> list = (List<Object>) responseMap.get("response");
	Object obj = list.get(2);
	System.out.println(obj);
	ObjectMapper oMapper = new ObjectMapper();
	Map<String, Object> map = oMapper.convertValue(obj, Map.class);
	String otpReference = map.get("otpReference").toString();
	restAssuredContext.getResponserMap().put("otpReference", otpReference);
	System.out.println("Test user1"+otpReference);
	context.setAttribute("otpReference", otpReference);
	System.out.println("Test user"+context.getAttribute("otpReference"));
	}
	}

